import math
from math import sqrt, log10, exp, cos
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches


def npa_conductive_heat(Time='1 giorno', T_min=10, T_max=20, Log_Z=1):
    
    Z=10**(Log_Z)
    Tm=0.5*(T_max+T_min)
    T0=T_max-Tm
    
    if Time == '1 giorno':
        max_time = 86400 
        Dt = 3600
        Nt= 24
    if Time == '1 anno':
        max_time = 31536000
        Dt = 86400
        Nt = 365
    if Time == '100K anni':
        max_time = 3153600000000
        Dt = 3153600000
        Nt = 1000
        
    print('Grafico della variazione di temperatura nel tempo, per una data profondita\':')
    print('Periodo:', Time, '(espresso in secondi: ', max_time, ' )')
    print('Profondita\' di osservazione in metri :', Z)

    t2=0.5*max_time
    
    rho=2.3*10**3
    cp=2.5*10**3
    kp=2.5
    
    omega=(2*3.14157)/max_time
    k=kp/(rho*cp)
        
    d=sqrt((2.0*k)/omega)
    
        
        
        
    x_all = []
    T_all = []
    Ts_all = []
    
    x=0
    i=0
    while i <= Nt:
        t=(i*Dt)
        x=t/max_time
        x_all.append(x) 
        Ts= Tm + T0*cos(omega*(t-t2))
        T = Tm + T0*exp(-1.0*(Z/d))*cos((omega*(t-t2))-(Z/d))
        T_all.append(T)
        Ts_all.append(Ts)
        i+=1
            
    depth=-1.0*d
    fig, (ax1,ax2) = plt.subplots(2,figsize=(15,6))
    ax1.plot(x_all,Ts_all)
    ax1.grid()
    ax1.set_ylabel('Temperatura esterna (gradi))')
    ax1.set_xlim(0,1)
    ax1.set_ylim(-20,40)
    
    ax2.grid()
    ax2.set_ylabel('Temperatura in profondita\' (gradi)')
    ax2.set_xlabel(' Tempo relativo in frazione del periodo in esame')
    ax2.plot(x_all,T_all,color='r')
    ax2.set_xlim(0,1)
    ax2.set_ylim(-20,40)
    plt.figure()
    
    return

